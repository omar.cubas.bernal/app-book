import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/shared/services/order.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-pay',
  templateUrl: './modal-pay.component.html',
  styleUrls: ['./modal-pay.component.scss']
})
export class ModalPayComponent implements OnInit {
  submitted = false;

  frmPay: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    last_name: ['', Validators.required],
    card: ['', Validators.required],
  })

  bagEmpty = false;
  total: any;
  
  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private orderService: OrderService,
    public router: Router,

    public dialogRef: MatDialogRef<ModalPayComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    this.total = this.data.amount;    
  }

  get f(){
    return this.frmPay.controls;
  }

  postPay(){
    this.submitted = true;

    var dataForPay:any = {};
    var detail:any = [];
    var { name, last_name } = this.frmPay.value;

    this.storageService.getProducts().forEach((element:any) => {
      detail.push({
        'product_id':element.id,
        'price':element.price,
        'quantity': element.bag[0].amount
      });
    });

    dataForPay = {
      'client': name + ' ' + last_name,
      'detail':detail
    }
    
    this.orderService.postOrder(dataForPay)
    .subscribe(
      (data:any) => {

        if(data.ready){
          
          Swal.fire(
            'Orden Generada!',
            'Gracias por la compra '+name + ' ' + last_name,
            'success'
          ).then((result) => {
            if (result.isConfirmed) {
              this.dialogRef.close();
              this.router.navigateByUrl('shop/products');
              localStorage.clear();
            }
      
          })

        }
        
      }
    )

  }
}
