import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Product } from 'src/app/pages/shop/products/interfaces/product';
import { ProductService } from 'src/app/pages/shop/products/services/product.service';
import { FilterService } from '../../services/filter.service';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})
export class CardProductComponent implements OnInit {
  showSpinner: boolean = false;

  products?:Product[] = [];

  public typeFilter:any;

  public searchFilter: any = '';
  query = '';
  clearInput = false;
  constructor(
    private productService: ProductService,
    private filterSerive: FilterService,
    private storageSerive: StorageService
  ) { }

  ngOnInit(): void {
    this.getProducts();
    this.filter();
  }

  getProducts(){
    this.productService.getToApi()
    .subscribe(
      (data:any) => {
        console.log("DATTTA:",data.data);
        this.products = data.data;
      }
    )
  }

  
  filter(){
    this.filterSerive.getFilters()
    .subscribe(
      (filter: any) =>{

        this.productService.getProducts()
        .subscribe(
          (data: any) => {
            
            this.products = data.filter(
              (element:any, index:any) =>{
                return element.category[0].id === filter[0].id;
              }
            );

          }
        )

      }
    )
  }
  
  handleChange(event: any){
    event.target.value.length > 0 ? this.clearInput = true : this.clearInput = false;
  }

  clearTextInput(){
    const input:any = document.getElementById("input-search");
    input.value = "";
    this.query = "";
    this.clearInput = false
  }

  addBag(id:String){
    var buttonAdd:any = document.getElementById(`btn_${id}`) as HTMLButtonElement;
    buttonAdd.disabled = true;

    this.createSpinner(id);
    setTimeout(() => {
      this.removeSpinner(id);
      this.addedProduct(id,'');
    }, 1000);
  }

  increase(id:String, max:Number){
    var buttonAdd:any = document.getElementById(`btn_${id}`) as HTMLButtonElement;
    
    this.addedProduct(id, 'edit');
    buttonAdd.disabled = false;

    var amount = document.getElementById(`amount_${id}`) as HTMLInputElement;
    
    if(max != parseInt(amount?.value)){
      amount.value = ( parseInt(amount?.value) + 1 ).toString();
    }else{
      return;
    }
  }

  diminish(id:String){
    
    var buttonAdd:any = document.getElementById(`btn_${id}`) as HTMLButtonElement;
    this.addedProduct(id, 'edit');
    buttonAdd.disabled = false;

    const amount = document.getElementById(`amount_${id}`) as HTMLInputElement;
    amount.value = ( (parseInt(amount?.value) > 1 ? parseInt(amount?.value) - 1 : 1) ).toString();
  }

  addedProduct(id:String, text:String){

    const btnAdd:any = document.querySelector(`#btn_${id}`);
    const amount = document.getElementById(`amount_${id}`) as HTMLInputElement;

    if(text === 'edit'){
      btnAdd.innerHTML = `<span> Agregar <ion-icon class="icon-add" name="add-circle-outline"></ion-icon></span>`;
    }else{

      const product = this.productService.getProductsById(id);
      product.forEach(
        (data:any) => {
          var bag = [];

          bag.push({
            'amount' : parseInt(amount?.value),
            'total' : data.price * parseInt(amount?.value)
          })

          data['bag'] = bag;

          this.productService.postBag(data);
        }
      )
      
      btnAdd.innerHTML = `<span> <ion-icon name="checkmark-outline"></ion-icon> Agregado</span>`;
    }

  }

  createSpinner(id:String){
    const btnAdd:any = document.querySelector(`#btn_${id}`);
    btnAdd.innerHTML = '';

    var createSpinner = document.createElement('div');
    createSpinner.setAttribute('id','spinner');

    btnAdd.appendChild(createSpinner);
  }

  removeSpinner(id:String){
    const btnAdd:any = document.querySelector(`#btn_${id}`);

    var spinner = document.querySelector("#spinner");
    btnAdd.removeChild(spinner);
  }

}
