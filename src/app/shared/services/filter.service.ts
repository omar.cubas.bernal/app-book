import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private filters = new Subject();

  constructor(
  ) {
   }

  setFilters(id: String){

    if(!this.validateStorage('filters')){
      this.changeFilter(id);
    }else{
      this.changeFilter(id);
    }
    
  }

  getFilters(){
    return this.filters.asObservable();
  }

  changeFilter(id: String){
    const arrayData = [];

    const data = {
      'id': id,
    }

    arrayData.push(data);
    const parseData = JSON.stringify(arrayData);
    localStorage.setItem("filters", parseData);

    const dataFilter:any = localStorage.getItem("filters");
    const dataFilterParse = JSON.parse(dataFilter);

    this.filters.next(dataFilterParse);
  }

  validateStorage(nameStorage: string){
    const filterStorage = localStorage.getItem(nameStorage);

    if(!filterStorage){
      return false
    }else{
      return true
    }
  }

}
