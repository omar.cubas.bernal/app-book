import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "./material/material.module";
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';


import { HeaderComponent } from "./component/header/header.component";
import { FooterComponent } from "./component/footer/footer.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { CardProductComponent } from './component/card-product/card-product.component';
import { SpinnerComponent } from './component/spinner/spinner.component';
import { GetCategoryPipe } from "../pipes/get-category.pipe";
import { ModalPayComponent } from './component/modals/modal-pay/modal-pay.component';
import { SearchFilterPipe } from "../pipes/search-filter.pipe";

@NgModule({
    declarations: [
        GetCategoryPipe,
        SearchFilterPipe,

        HeaderComponent,
        FooterComponent,
        CardProductComponent,
        SpinnerComponent,
        ModalPayComponent
    ],
    imports: [
        NgxQRCodeModule,
        BrowserAnimationsModule,
        MaterialModule,
        BrowserModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,

        IonicModule.forRoot()
    ],
    exports:[
        MaterialModule,
        HeaderComponent,
        FooterComponent,
        CardProductComponent,
        SpinnerComponent


    ]
})
export class SharedModule{}