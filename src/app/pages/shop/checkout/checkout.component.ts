import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalPayComponent } from 'src/app/shared/component/modals/modal-pay/modal-pay.component';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  products: any = [];
  total: any;

  bagEmpty = false;

  constructor(
    public dialog: MatDialog,
    private storageService: StorageService
  ) { }

  ngOnInit(): void {
    this.getProducts();
    this.getProductsInBag();
    
    this.getPrice();
    this.getPriceTotal();
  }

  
  getProductsInBag(){
    this.products = this.storageService.getProducts();

    if(!this.products){
      this.bagEmpty = true;
    }
    
  }

  getProducts(){
    this.storageService.getProdctsToBag()
    .subscribe(
      (data:any) => {
        console.log(data);
        
        this.products = this.storageService.getProducts();
      }
    )
  }

  getPrice(){
    this.storageService.getPrice()
    .subscribe(
      (data:any) => {
        this.total = data;
      }
    )
  }

  getPriceTotal(){
    try {
      this.storageService.getPriceTotal()
      ?.subscribe(
        (data:any) => {
          this.total = data;
        }
      );

    } catch (error) {
      this.bagEmpty = true;
    }
  
  }

  removeProduct(id: String){
    this.storageService.deleteProductToStorage(id);
  }


  frmPay(){
    const dialogRef = this.dialog.open(
      ModalPayComponent,{
      width: '400px',
        data: {
          amount: this.total,
        }
      }
      );

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
    
  }
}
