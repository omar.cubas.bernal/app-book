import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ModalPayComponent } from 'src/app/shared/component/modals/modal-pay/modal-pay.component';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ProductService } from '../products/services/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  products: any = [];
  total: any;

  bagEmpty = false;
  constructor(
    private _router: Router,
    public dialog: MatDialog,


    /* Principal */
    private storageService: StorageService,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.getProducts();
    this.getProductsInBag();
    
    this.getPrice();
    this.getPriceTotal();
  }


  frmPay(){
    const dialogRef = this.dialog.open(
      ModalPayComponent,{
      width: '400px',
        data: {
          amount: this.total,
        }
      }
      );

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
    
  }

  getProductsInBag(){

    this.products = this.storageService.getProducts();
      
    if(!this.products){
      this.bagEmpty = true;
    }
    
  }

  getProducts(){
    this.storageService.getProdctsToBag()
    .subscribe(
      (data:any) => {
        this.bagEmpty = false;
        
        this.products = data;
      }
    )
  }

  getPrice(){
    this.storageService.getPrice()
    .subscribe(
      (data:any) => {
        this.total = data;
      }
    )
  }

  getPriceTotal(){
    try {
      this.storageService.getPriceTotal()
      ?.subscribe(
        (data:any) => {
          this.total = data;
        }
      );

    } catch (error) {
      this.bagEmpty = true;
    }
  
  }

  removeProduct(id: String){
    this.storageService.deleteProductToStorage(id);
  }

}
