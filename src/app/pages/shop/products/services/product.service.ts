import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StorageService } from 'src/app/shared/services/storage.service';
import { environment } from 'src/environments/environment';

import Products from '../../../../data/Products.json';
import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private env =  environment;

  private products$!: Observable<any>;
  private products = new Subject();


  constructor(
    private http: HttpClient,

    private storageService: StorageService
  ) { }


  getToApi(){
    return this.http.get(this.env.api + `/products`, {
      headers: new HttpHeaders({
        'content-type':'application/json'
      })
    })
    .pipe(catchError(this.handlingError));
  }

  

  getProducts(){
    return this.http.get(this.env.api + `/products`, {
      headers: new HttpHeaders({
        'content-type':'application/json'
      })
    })
    .pipe(catchError(this.handlingError));
    /*this.products$ = new Observable(
      ( suscribe => {
        suscribe.next(Products);
      })
    )
    return this.products$;*/
  }

  private handlingError(error: HttpErrorResponse){
    if(error.error instanceof ErrorEvent){
      console.warn('Error en el Front:', error.error.message)
    }else{
      console.warn('Error en el Back:', error.status, error.error.message)
    }
    return throwError( ()=> 'Error de comunicación HTTP');
  }


  getProductsById(id: String){
    const product = Products.filter(
      (element:any, index:any) => {
        return element.id === parseInt(id+'');
      }
    )
    return product;
  }

  postBag(product: Product){
    const dataStorage = localStorage.getItem("bag");
    var productsInBag = [];

    if(dataStorage){
      
      const dataParse = JSON.parse(dataStorage);
      const dataString = JSON.stringify(product);
      
      if(dataParse?.length > 0){

        const dataStorage = dataParse.filter(
          (element:any, index:any) => {
            return element.id != product.id;
          }
        )

        dataStorage.forEach(
          (data:any) => {
            productsInBag.push(data);
          }
        );
        
        localStorage.removeItem('bag');
        productsInBag.push(product);

        const productsInBagStringify = JSON.stringify(productsInBag);
        localStorage.setItem('bag', productsInBagStringify);

      }else{

        if(dataParse.id === product.id){
          localStorage.setItem('bag', dataString);
        }else{
          
          productsInBag.push(dataParse);
          productsInBag.push(product);

          const productsInBagStringify = JSON.stringify(productsInBag);
          localStorage.setItem('bag', productsInBagStringify);
        }
      }
      
    }else{
      var array = [];
      array.push(product);

      const dataString = JSON.stringify(array);
      localStorage.setItem('bag', dataString);

    }
    
    
    const storage:any = localStorage.getItem("bag");
    const storageParse = JSON.parse(storage);
    
    const storageParseLength:any = storageParse ? storageParse.hasOwnProperty(length) ? storageParse.length : 1 : 0;

    this.storageService.changeCountBag(storageParseLength);

    this.storageService.changeProductsBag(storageParse);

    this.storageService.getPrice();
  }
  
}
