export interface Product {
    id: String,
    name: String,
    price: String,
    image: String,
    sku: String,
    description: String,
}
