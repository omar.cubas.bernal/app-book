import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PagesComponent } from "./pages.component";
import { CheckoutComponent } from "./shop/checkout/checkout.component";
import { ProductsComponent } from "./shop/products/products.component";
import { ShopComponent } from "./shop/shop.component";

const routes: Routes = [
    {
        path: '',
        redirectTo: 'tienda/productos',
        pathMatch: 'full'
    },
    { path: '', component: PagesComponent,
        children: [
            { path: 'tienda', component: ShopComponent,
                children: [
                    { path: 'productos', component: ProductsComponent },
                    { path: 'checkout', component: CheckoutComponent },
                ] 
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule {}